#importação das bibliotecas utilizadas
from selenium import webdriver #!pip install selenium
from selenium.webdriver.common.by import By


from unidecode import unidecode
import copy
import locale
import re
import time
from datetime import date
import pymongo
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from selenium import webdriver


client = pymongo.MongoClient("mongodb+srv://eduardocdiogenes:ikCf850j6u8Pvkwn@cestacheia.9nwfrw2.mongodb.net/?retryWrites=true&w=majority")
db = client.cestaCheia

def get_text_and_image(driver):

    products = []
    model = {"Texto":"","Imagem":""}

    #para o pao de açucar temos esse elemento gigantesco com todos os produtos
    for i in driver.find_elements(By.CLASS_NAME,"product-cardstyles__Container-sc-1uwpde0-1"):
        modelo_novo = copy.deepcopy(model)
        try:
            modelo_novo["Texto"] = i.text
            modelo_novo["Imagem"] = i.find_element(By.TAG_NAME,"a").get_attribute("src")
            print(i.find_element(By.TAG_NAME,"a").get_attribute("src"))
            products.append(modelo_novo)
        except:
            pass

    return products

def cosine_string_similarity(str1, str2):
    vectorizer = CountVectorizer().fit_transform([str1, str2])
    vectors = vectorizer.toarray()
    cosine_sim = cosine_similarity(vectors)
    return cosine_sim[0][1]

def scroll_to_end(number_pages,driver, scroll_pause_time):

    # Pegue a altura inicial da página

    y = 0
    contador = 500
    all_products = []
    pages = []

    while True:

        Start_url = driver.current_url
        y = driver.execute_script("return document.body.scrollHeight;") 
        print(Start_url)
        driver.execute_script("window.scrollTo(0, "+str(y)+")")
        time.sleep(scroll_pause_time) 

        End_url = driver.current_url

        #current_page = driver.current_url.split("p=")[-1].split("&qt=")[0

        if Start_url == "https://www.paodeacucar.com/categoria/alimentos?p={}&qt=80".format(number_pages):
            break
        
        elif Start_url == End_url:

            time.sleep(scroll_pause_time)
            y = y * 0.9
            driver.execute_script("window.scrollTo(0, "+str(y)+")")
    
    #all_products += get_text_and_image(driver)
    return driver


        

def coleta_pao_de_acucar():

    url = "https://www.paodeacucar.com/categoria/alimentos?p=1&qt=80"

    # Exemplo de uso:
    driver = webdriver.Chrome('Chrome\chromedriver.exe')
    #edge_options = webdriver.Edge


    driver.get(url)
    driver.maximize_window()
    time.sleep(10)

    pattern = r"Alimentos: encontre aqui (.*?) produtos em promoção"
    match = re.search(pattern,driver.find_element_by_tag_name('body').text.replace("\n"," "))
    #print(driver.find_element_by_tag_name('body').text)
    if match:
        number_pages = int(int(match.group(1))/80) + 1
    else:
        number_pages = 100


    all_products = scroll_to_end(number_pages,driver,scroll_pause_time=1)

    Texto_Links = []
    for i in range(1,int(match.group(1))):
        element = driver.find_element(By.XPATH,"/html/body/div[1]/div[1]/div/div[4]/div[4]/div/div[2]/div[2]/div[{}]".format(i))
        driver.execute_script("window.scrollTo(100,{});".format(element.location['y']+200))
        time.sleep(0.15)
        try:
            Texto_Links.append({"Text":element.text,"Link":element.find_element(By.TAG_NAME,"img").get_attribute("src")})
        except:
            pass

    model = {'Name':'','Taxonomy':'','Brand':'','Supermarket':[
    {'Name':'Pão de Açucar', 'Price':'', 'Discount':0.0,'Date':str(date.today()),'Image':'', 'Historic':[
        {'Price':'','Discount':0.0,'Date':str(date.today())}
        ]}]}

    Nomes = []
    Todos_Produtos = []
    for i in Texto_Links:
        json_ = copy.deepcopy(model)
        lista_itens = i["Text"].split("\n")
        last_occurrence = max((i for i in range(len(lista_itens)) if 'R$' in lista_itens[i]), default=-1)
        first_occurrence = next((i for i, item in enumerate(lista_itens) if 'R$' in item), -1)

        padrao_percentage = r"(\d+(?:\.\d+)?)%"
        match = re.search(padrao_percentage, i["Text"])

        if match:

            json_["Supermarket"][-1]['Discount'] = float(float(match.group(1)) / 100.0)
            json_["Supermarket"][-1]['Historic'][-1]['Discount'] = float(float(match.group(1)) / 100.0)

        padrao_valor = r"R\$(\d+,\d{2})"
        resultado = re.search(padrao_valor, lista_itens[last_occurrence])
        
        if resultado:
            json_["Supermarket"][-1]['Price'] = float((resultado.group(1).replace(',', '.')))
            json_["Supermarket"][-1]['Historic'][-1]['Price'] = float((resultado.group(1).replace(',', '.')))
            json_["Supermarket"][-1]['Image'] = i['Link']

            if 'OCP: /' in lista_itens:
                lista_itens.remove('OCP: /')

            json_["Name"] = lista_itens[0].title()
        
        if json_["Name"] not in Nomes:
            Nomes.append(json_["Name"].title())
            Todos_Produtos.append(json_)

    marcas = list(db['Brands'].find({}))

    for i in range(len(Todos_Produtos)):
        for j in marcas:
            if " "+j['Brand']+" " in Todos_Produtos[i]['Name'].lower():
                print(j,Todos_Produtos[i]['Name'])
                Todos_Produtos[i]['Brand'] = j['Brand']

    def cosine_string_similarity(str1, str2):
        vectorizer = CountVectorizer().fit_transform([str1, str2])
        vectors = vectorizer.toarray()
        cosine_sim = cosine_similarity(vectors)
        return cosine_sim[0][1]

    itens = []

    for i in unique_jsons_list:

        lista_similares = []

        Produto_igual = db['Products'].find_one({'Name': i['Name']})

        if Produto_igual:
            id_igual = Produto_igual['_id']

            for j in range(len(Produto_igual['Supermarket'])):
                if Produto_igual['Supermarket'][j]['Name'] == i['Supermarket'][0]['Name']:
                    Produto_igual['Supermarket'][j]['Date'] = i['Supermarket'][0]['Date']
                    Produto_igual['Supermarket'][j]['Price'] = i['Supermarket'][0]['Price']
                    Produto_igual['Supermarket'][j]['Discount'] = i['Supermarket'][0]['Discount']
                    Produto_igual['Supermarket'][j]['Historic'].append(i['Supermarket'][0]['Historic'][0])

                    filter = {'_id': Produto_igual['_id']}
                    print(i)
                    db['Products'].update_one(filter, {"$set" : {'Supermarket':Produto_igual['Supermarket']}})


        elif i['Brand'] != 'Outros':

            for dados_bd in db['Products'].find({'Brand': i['Brand'].lower()}):
                
                match1 = re.findall(r'(\d+)', dados_bd['Name'])
                match2 = re.findall(r'(\d+)', i['Name'])

                sim = cosine_string_similarity(i['Name'].lower(),dados_bd['Name'].lower())

                if sim > 0.60:
                    if set(match1) & set(match2):
                        lista_similares.append({"Produto":dados_bd,"Sim":sim})
                    elif len(match1) == 0 and len(match2) == 0:
                        lista_similares.append({"Produto":dados_bd,"Sim":sim})

            itens.append([i,lista_similares])
        else:

            itens.append([i,lista_similares])

    for i in range(len(itens)):
        for j in range(i+1, len(itens)):
            for k in range(len(itens[i][1])):
                for l in range(len(itens[j][1])):
                    if itens[i][1][k]["Produto"] == itens[j][1][l]["Produto"]:
                        if itens[i][1][k]["Sim"] >= itens[j][1][l]["Sim"]:
                            itens[j][1][l]["Sim"] = 0
                        else:
                            itens[i][1][k]["Sim"] = 0

    itens_upload = []

    for i in itens:
        i_copy = copy.deepcopy(i)  # cria uma cópia profunda de i
        if i_copy[1] == []:
            itens_upload.append(i_copy[0])
        else:
            highest = 0
            aux = ''
            same = False
            for j in i_copy[1]:
                if j['Sim'] > highest:
                    aux = j
                    highest = j['Sim'] 

            if not isinstance(aux, str):

                filter = {'_id': aux['Produto']['_id']}
                Supermarket = aux['Produto']['Supermarket']

                for j in range(len(Supermarket)):
                    if Supermarket[j]['Name'] == i_copy[0]['Supermarket'][0]['Name']:
                        Supermarket[j]['Historic'].append(i_copy[0]['Supermarket'][0]['Historic'][0])
                        Supermarket[j]['Date'] = i_copy[0]['Supermarket'][0]['Date']
                        Supermarket[j]['Price'] = i_copy[0]['Supermarket'][0]['Price']
                        same = True
                        break

                if not same:
                    Supermarket.append(i_copy[0]['Supermarket'][0])
                    break

                db['Products'].update_one(filter, {"$set" : {'Supermarket':Supermarket}})


    db['Products'].insert_many(itens_upload)