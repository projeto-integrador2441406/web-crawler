from lxml import html
import requests
from lxml import etree
from bs4 import BeautifulSoup
import csv
import pandas as pd
import string
import pymongo
from datetime import date
import copy
import json
import copy


# from msedge.selenium_tools import Edge, EdgeOptions
from selenium.webdriver.common.by import By
from selenium import webdriver
import time
from unidecode import unidecode
# from msedge.selenium_tools import Edge, EdgeOptions
from selenium.webdriver.common.by import By
from selenium import webdriver
import time
import re

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity

def cosine_string_similarity(str1, str2):
    vectorizer = CountVectorizer().fit_transform([str1, str2])
    vectors = vectorizer.toarray()
    cosine_sim = cosine_similarity(vectors)
    return cosine_sim[0][1]

def coleta_sao_luiz():

    client = pymongo.MongoClient("mongodb+srv://eduardocdiogenes:ikCf850j6u8Pvkwn@cestacheia.9nwfrw2.mongodb.net/?retryWrites=true&w=majority")
    db = client.cestaCheia

    # Inicializar o WebDriver
    #path do driver
    driver = webdriver.Chrome('Chrome\chromedriver.exe')
    driver.get("https://www.mercadinhossaoluiz.com.br")

    time.sleep(5)


    # pega os primeiros links
    itens = driver.find_element(By.XPATH,
                                "/html/body/div[2]/div/div[1]/div/div[4]/div/div[1]/section/div/div[1]/nav/section/ul/li/div")
    links_iniciais = []
    links_pesquisados = []
    for i in itens.find_elements(By.TAG_NAME, "a"):
        if i.get_attribute("href").count("/") == 3:
            if 'nao-alimentos' in i.get_attribute("href"):
                break
            links_iniciais.append(i.get_attribute("href"))
            links_pesquisados.append(i.get_attribute("href") + "?order=OrderByNameDESC")
            links_pesquisados.append(i.get_attribute("href") + "?order=OrderByNameASC")

    links_pesquisados

    marcas = []

    for i in links_iniciais:
        driver.get(i)
        posicao = i[i.rfind("/") + 1:]

        time.sleep(2)
        marcas += driver.find_element(By.XPATH,
                                    '/html/body/div[2]/div/div[1]/div/div[5]/div/div/section/div[2]/div/div/section/div/div[1]/div/div/div/div/div[4]/div[2]').text.split(
            "\n")


    marcas_ = []
    for i in marcas:
        marcas_.append(i.title())

    db['Brands'].find_one({})

    valores_do_dia = []

    today = date.today()
    start_time = time.time()

    model = {'Name': '', 'Taxonomy': '', 'Brand': '', 'Supermarket': [
        {'Name': 'São Luiz', 'Price': '', 'Discount': 0.0, 'Date': str(date.today()), 'Image': '', 'Historic': [
            {'Price': '', 'Discount': 0.0, 'Date': str(date.today())}
        ]}]}

    for link_ in range(len(links_pesquisados)):
        print(link_)
        for i in range(1, 51):

            link = links_pesquisados[link_] + "&page={}".format(i)
            driver.get(link)
            time.sleep(1.5)
            try:
                itens_no_grid = driver.find_element(By.ID, "gallery-layout-container")
            except:
                pass
            else:
                for Elemento in itens_no_grid.find_elements(By.TAG_NAME, "a"):
                    modelo_novo = copy.deepcopy(model)

                    modelo_novo["Name"] = Elemento.find_element(By.TAG_NAME, "h3").text.title()
                    modelo_novo['Supermarket'][-1]['Image'] = Elemento.find_element(By.TAG_NAME, "img").get_attribute("src")

                    try:
                        modelo_novo['Supermarket'][-1]['Historic'][-1]['Price'] = float(
                            Elemento.find_element(By.CLASS_NAME, "vtex-product-price-1-x-currencyContainer").text.replace(
                                "R$", "").replace(",", ".").strip())
                        modelo_novo['Supermarket'][-1]['Price'] = float(
                            Elemento.find_element(By.CLASS_NAME, "vtex-product-price-1-x-currencyContainer").text.replace(
                                "R$", "").replace(",", ".").strip())
                    except:
                        pass

                    try:

                        modelo_novo['Supermarket'][-1]['Discount'] = float("0." + Elemento.find_element(By.CLASS_NAME,
                                                                                                        "vtex-store-components-3-x-discountInsideContainer").text.replace(
                            "%\nOFF", ""))
                        modelo_novo['Supermarket'][-1]['Discount'] = float("0." + Elemento.find_element(By.CLASS_NAME,
                                                                                                        "vtex-store-components-3-x-discountInsideContainer").text.replace(
                            "%\nOFF", ""))
                    except:
                        pass

                    if modelo_novo['Supermarket'][-1]['Price'] != "":
                        for i in marcas:
                            try:
                                match = re.search(r'\b{}\b'.format(i.lower()), modelo_novo["Name"].lower())
                            except:
                                pass
                            else:
                                if match:
                                    modelo_novo["Brand"] = i

                        valores_do_dia.append(modelo_novo)


    marcas = list(db['Brands'].find({}))
    marcas

    marcas = list(db['Brands'].find({}))

    produtos = valores_do_dia

    for i in range(len(produtos)):
        for j in marcas:
            try:
                match = re.findall(r'\b{}\b'.format(unidecode(j['Brand'].lower())), unidecode(produtos[i]['Name'].lower()))
            except:
                pass
            else:
                if match:
                    break
        if match:
            produtos[i]['Brand'] = match[0]
        else:
            produtos[i]['Brand'] = 'Outros'

    str_jsons_list = [json.dumps(item, sort_keys=True) for item in produtos]

    # Remova duplicatas convertendo para um set e depois de volta para uma lista:
    unique_str_jsons_list = list(set(str_jsons_list))

    # Converta cada string de volta para um JSON:
    unique_jsons_list = [json.loads(item) for item in unique_str_jsons_list]


    itens = []

    for i in str_jsons_list:

        i = json.loads(i)
        lista_similares = []
        if i['Brand'] != 'Outros':
            print(i)
            for dados_bd in db['Products'].find({'Brand': i['Brand']}):
                print(len(dados_bd))
                match1 = re.findall(r'(\d+)', dados_bd['Name'])
                match2 = re.findall(r'(\d+)', i['Name'])

                sim = cosine_string_similarity(i['Name'],dados_bd['Name'])
                if sim > 0.60:
                    if set(match1) & set(match2):
                        lista_similares.append({"Produto":dados_bd,"Sim":sim})
                    elif len(match1)>= 1 or len(match2)>= 1:
                        lista_similares.append({"Produto":dados_bd,"Sim":sim})

        if len(lista_similares) > 0:
            highest = 0
            aux = ''
            for j in lista_similares:
                if j['Sim'] > highest:
                    aux = j
                    highest = j['Sim']

            filter = {'_id': j["Produto"]["_id"]}
            item = db['Products'].find_one(filter)
            item['Supermarket'].append(aux['Produto']['Supermarket'][-1])
            db['Products'].update_one(filter, {"$set" : {'Supermarket':item['Supermarket']}})

            print(i['Name'])
            print(aux['Produto']['Name'])
            #print(db['Products'].find_one(filter))
            print("----")
        else:
            itens.append(i)

    for i in range(len(itens)):
        for j in range(i+1, len(itens)):
            for k in range(len(itens[i][1])):
                for l in range(len(itens[j][1])):
                    if itens[i][1][k]["Produto"] == itens[j][1][l]["Produto"]:
                        if itens[i][1][k]["Sim"] >= itens[j][1][l]["Sim"]:
                            itens[j][1][l]["Sim"] = 0
                        else:
                            itens[i][1][k]["Sim"] = 0

        itens_upload = []

    for i in itens:
        i_copy = copy.deepcopy(i)  # cria uma cópia profunda de i
        if i_copy[1] == []:
            itens_upload.append(i_copy[0])
        else:
            highest = 0
            aux = ''
            same = False
            for j in i_copy[1]:
                if j['Sim'] > highest:
                    aux = j
                    highest = j['Sim'] 

            if not isinstance(aux, str):

                filter = {'_id': aux['Produto']['_id']}
                Supermarket = aux['Produto']['Supermarket']

                for j in range(len(Supermarket)):
                    if Supermarket[j]['Name'] == i_copy[0]['Supermarket'][0]['Name']:
                        Supermarket[j]['Historic'].append(i_copy[0]['Supermarket'][0]['Historic'][0])
                        Supermarket[j]['Date'] = i_copy[0]['Supermarket'][0]['Date']
                        Supermarket[j]['Price'] = i_copy[0]['Supermarket'][0]['Price']
                        same = True
                        break

                if not same:
                    Supermarket.append(i_copy[0]['Supermarket'][0])
                    break

                db['Products'].update_one(filter, {"$set" : {'Supermarket':Supermarket}})

    db['Products'].insert_many(itens_upload)