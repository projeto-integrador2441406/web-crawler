#importação das bibliotecas utilizadas
from selenium import webdriver #!pip install selenium
from selenium.webdriver.common.by import By

from selenium import webdriver
from unidecode import unidecode
import copy
import locale
import re
import time
from datetime import date
import copy
import json
import pymongo
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity

def cosine_string_similarity(str1, str2):
    vectorizer = CountVectorizer().fit_transform([str1, str2])
    vectors = vectorizer.toarray()
    cosine_sim = cosine_similarity(vectors)
    return cosine_sim[0][1]

def wait_for_page_to_load(driver, timeout=30):
    WebDriverWait(driver, timeout).until(
        lambda d: d.execute_script('return document.readyState') == 'complete'
    )

def scroll_to_end(driver, scroll_pause_time):

    while True:

        y_start = driver.execute_script("return document.body.scrollHeight;") 
        driver.execute_script("window.scrollTo(0, "+str(y_start)+")")
        time.sleep(scroll_pause_time) 
        y_end = driver.execute_script("return document.body.scrollHeight;") 

        if y_start == y_end:
            break

    return driver

def coleta_superlagoa():

    client = pymongo.MongoClient("mongodb+srv://eduardocdiogenes:ikCf850j6u8Pvkwn@cestacheia.9nwfrw2.mongodb.net/?retryWrites=true&w=majority")
    db = client.cestaCheia

    #Nao coletados:Linha é Pra Já, Higiente, Limpeza

    Super_Lagoa_Pages = ["https://comprasonline.superlagoa.com.br/loja/262/categoria/4204",
                        "https://comprasonline.superlagoa.com.br/loja/262/categoria/4172",
                        "https://comprasonline.superlagoa.com.br/loja/262/categoria/4152",
                        "https://comprasonline.superlagoa.com.br/loja/262/categoria/4166",
                        "https://comprasonline.superlagoa.com.br/loja/262/categoria/15229",
                        "https://comprasonline.superlagoa.com.br/loja/262/categoria/4156",
                        "https://comprasonline.superlagoa.com.br/loja/262/categoria/4161",
                        "https://comprasonline.superlagoa.com.br/loja/262/categoria/4218",
                        "https://comprasonline.superlagoa.com.br/loja/262/categoria/4178",
                        "https://comprasonline.superlagoa.com.br/loja/262/categoria/4222",
                        "https://comprasonline.superlagoa.com.br/loja/262/categoria/4182",
                        "https://comprasonline.superlagoa.com.br/loja/262/categoria/4226",
                        "https://comprasonline.superlagoa.com.br/loja/262/categoria/4232",
                        "https://comprasonline.superlagoa.com.br/loja/262/categoria/4209",
                        "https://comprasonline.superlagoa.com.br/loja/262/categoria/4193",
                        "https://comprasonline.superlagoa.com.br/loja/262/categoria/4246",
                        "https://comprasonline.superlagoa.com.br/loja/262/categoria/4249",
                        "https://comprasonline.superlagoa.com.br/loja/262/categoria/4187",
                        "https://comprasonline.superlagoa.com.br/loja/262/categoria/13826",
                        "https://comprasonline.superlagoa.com.br/loja/262/categoria/14439"
                        ]

    produtos = []

    model = {'Name':'','Taxonomy':'','Brand':'','Supermarket':[
        {'Name':'Super Lagoa', 'Price':'', 'Discount':0.0,'Date':str(date.today()),'Image':'', 'Historic':[
            {'Price':'','Discount':0.0,'Date':str(date.today())}
            ]}]}

    pattern = r"R\$([\d]+),([\d]{2})"
    replacement = r"\1.\2"

    driver = webdriver.Chrome('Chrome\chromedriver.exe')
    time.sleep(10)
    driver.get(Super_Lagoa_Pages[0])


    for pagina in Super_Lagoa_Pages:
        driver.get(pagina)
        driver.execute_script('window.location.href = "{}";'.format(pagina))
        driver.maximize_window()
        time.sleep(20)
        scroll_to_end(driver, 1)

        for i in driver.find_elements(By.XPATH,"/html/body/div[1]/div/section/div/div/div[3]/div[1]/div/div/div/div/div"):

            texto_separado = i.text.split("\n")
            tipo = len(texto_separado)
            model_copy = copy.deepcopy(model)
            match tipo:
                case 2 | 6:

                    model_copy['Name'] = texto_separado[1]
                    model_copy['Supermarket'][-1]['Price'] = float(re.sub(pattern, replacement, texto_separado[0]))
                    model_copy['Supermarket'][-1]['Image'] = i.find_element(By.TAG_NAME,"img").get_attribute("src")
                    model_copy['Supermarket'][-1]['Historic'][-1]['Price'] = float(re.sub(pattern, replacement, texto_separado[0]))
                    
                    #print({"Price":float(re.sub(pattern, replacement, texto_separado[0])),"Product_Name":texto_separado[1],"Product_Url":i.find_element(By.TAG_NAME,"img").get_attribute("src")})
                case 4:
                    
                    try:
                        model_copy['Name'] = texto_separado[3]
                        model_copy['Supermarket'][-1]['Price'] = float(re.sub(pattern, replacement, texto_separado[1]))
                        model_copy['Supermarket'][-1]['Image'] = i.find_element(By.TAG_NAME,"img").get_attribute("src")
                        model_copy['Supermarket'][-1]['Historic'][-1]['Price'] = float(re.sub(pattern, replacement, texto_separado[1]))
                    except:
                        model_copy['Name'] = texto_separado[1]
                        model_copy['Supermarket'][-1]['Price'] = float(re.sub(pattern, replacement, texto_separado[0]))
                        model_copy['Supermarket'][-1]['Image'] = i.find_element(By.TAG_NAME,"img").get_attribute("src")
                        model_copy['Supermarket'][-1]['Historic'][-1]['Price'] = float(re.sub(pattern, replacement, texto_separado[0]))


                    #print({"Price":float(re.sub(pattern, replacement, texto_separado[1])),"Product_Name":texto_separado[3],"Product_Url":i.find_element(By.TAG_NAME,"img").get_attribute("src")})
                case 3 | 7:

                    model_copy['Name'] = texto_separado[2]
                    model_copy['Supermarket'][-1]['Price'] = float(re.sub(pattern, replacement, texto_separado[1]))
                    model_copy['Supermarket'][-1]['Image'] = i.find_element(By.TAG_NAME,"img").get_attribute("src")
                    model_copy['Supermarket'][-1]['Historic'][-1]['Price'] = float(re.sub(pattern, replacement, texto_separado[1]))


            produtos.append(model_copy)

    for i in range(len(produtos)):
        produtos[i]['Name'] = unidecode(produtos[i]['Name'].title())

    marcas = list(db['Brands'].find({}))

    for i in range(len(produtos)):
        for j in marcas:
            try:
                match = re.findall(r'\b{}\b'.format(j['Brand'].lower()), produtos[i]['Name'].lower())
            except:
                pass
            else:
                if match:
                    break
        if match:
            produtos[i]['Brand'] = match[0]
        else:
            produtos[i]['Brand'] = 'outros'
            
        produtos[i]['Name'] = produtos[i]['Name'].title()

    # Converta cada JSON em uma string:
    str_jsons_list = [json.dumps(item, sort_keys=True) for item in produtos]

    # Remova duplicatas convertendo para um set e depois de volta para uma lista:
    unique_str_jsons_list = list(set(str_jsons_list))

    # Converta cada string de volta para um JSON:
    unique_jsons_list = [json.loads(item) for item in unique_str_jsons_list]

    itens = []

    for i in unique_jsons_list:

        lista_similares = []

        Produto_igual = db['Products'].find_one({'Name': i['Name']})

        if Produto_igual:
            id_igual = Produto_igual['_id']

            for j in range(len(Produto_igual['Supermarket'])):
                if Produto_igual['Supermarket'][j]['Name'] == i['Supermarket'][0]['Name']:
                    Produto_igual['Supermarket'][j]['Date'] = i['Supermarket'][0]['Date']
                    Produto_igual['Supermarket'][j]['Price'] = i['Supermarket'][0]['Price']
                    Produto_igual['Supermarket'][j]['Discount'] = i['Supermarket'][0]['Discount']
                    Produto_igual['Supermarket'][j]['Historic'].append(i['Supermarket'][0]['Historic'][0])

                    filter = {'_id': Produto_igual['_id']}
                    #print(i)
                    db['Products'].update_one(filter, {"$set" : {'Supermarket':Produto_igual['Supermarket']}})


        elif i['Brand'] != 'Outros':

            for dados_bd in db['Products'].find({'Brand': i['Brand'].lower()}):
                
                match1 = re.findall(r'(\d+)', dados_bd['Name'])
                match2 = re.findall(r'(\d+)', i['Name'])

                sim = cosine_string_similarity(i['Name'].lower(),dados_bd['Name'].lower())

                if sim > 0.75:
                    if set(match1) & set(match2):
                        lista_similares.append({"Produto":dados_bd,"Sim":sim})
                    elif len(match1) == 0 and len(match2) == 0:
                        lista_similares.append({"Produto":dados_bd,"Sim":sim})
                        

            itens.append([i,lista_similares])
        else:

            itens.append([i,lista_similares])

    for i in range(len(itens)):
        for j in range(i+1, len(itens)):
            for k in range(len(itens[i][1])):
                for l in range(len(itens[j][1])):
                    if itens[i][1][k]["Produto"] == itens[j][1][l]["Produto"]:
                        if itens[i][1][k]["Sim"] >= itens[j][1][l]["Sim"]:
                            itens[j][1][l]["Sim"] = 0
                        else:
                            itens[i][1][k]["Sim"] = 0


    itens_upload = []

    for i in itens:
        i_copy = copy.deepcopy(i)  # cria uma cópia profunda de i
        if i_copy[1] == []:
            itens_upload.append(i_copy[0])
        else:
            highest = 0
            aux = ''
            same = False
            for j in i_copy[1]:
                if j['Sim'] > highest:
                    aux = j
                    highest = j['Sim'] 

            if not isinstance(aux, str):

                filter = {'_id': aux['Produto']['_id']}
                Supermarket = aux['Produto']['Supermarket']

                for j in range(len(Supermarket)):
                    if Supermarket[j]['Name'] == i_copy[0]['Supermarket'][0]['Name']:
                        Supermarket[j]['Historic'].append(i_copy[0]['Supermarket'][0]['Historic'][0])
                        Supermarket[j]['Date'] = i_copy[0]['Supermarket'][0]['Date']
                        Supermarket[j]['Price'] = i_copy[0]['Supermarket'][0]['Price']
                        same = True
                        break

                if not same:
                    Supermarket.append(i_copy[0]['Supermarket'][0])
                    break

                db['Products'].update_one(filter, {"$set" : {'Supermarket':Supermarket}})


    db['Products'].insert_many(itens_upload)