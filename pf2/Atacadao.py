#importação das bibliotecas utilizadas
# from selenium import webdriver #!pip install selenium
import undetected_chromedriver as uc
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

import math
from selenium import webdriver
from unidecode import unidecode
import copy
import locale
import re
import time
from datetime import date
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import copy
import pymongo

def cosine_string_similarity(str1, str2):
    vectorizer = CountVectorizer().fit_transform([str1, str2])
    vectors = vectorizer.toarray()
    cosine_sim = cosine_similarity(vectors)
    return cosine_sim[0][1]


def coleta(driver,elemento_maior):

    data = []

    model = {'Name':'','Taxonomy':'','Brand':'','Supermarket':[
    {'Name':'Atacadão', 'Price':'', 'Discount':0.0,'Date':str(date.today()),'Image':'', 'Historic':[
        {'Price':'','Discount':0.0,'Date':str(date.today())}
        ]}]}

    for i in driver.find_elements(By.XPATH,'/html/body/main/section[3]/div[1]/div[2]/div[2]/div[4]/div/div')[elemento_maior-6:elemento_maior]:
        textos = i.text.split("\n")

        modelo_atual = copy.deepcopy(model)

        if 'Economia de' in textos[0]:
            
            result = re.findall(r'(\d+)%', textos[0])
            modelo_atual['Supermarket'][-1]['Discount'] = float(100/float(result[0]))
            modelo_atual['Supermarket'][-1]['Historic'][-1]['Discount'] = float(100/float(result[0]))
            textos.pop(0)

        valor_final = False
        for texto_separado in textos:
            if 'R$' in texto_separado:
                Valor_produto = re.search(r'\d+,\d+', texto_separado).group(0).replace(',', '.')
                valor_final = True
        
        if valor_final:
            modelo_atual['Name'] = textos[0].title()

            marca = textos[0].rsplit("-", 1)

            if len(marca) > 1:
                modelo_atual['Brand'] = textos[0].rsplit("-", 1)[-1].strip()
            modelo_atual['Supermarket'][-1]['Price'] = float(Valor_produto)
            modelo_atual['Supermarket'][-1]['Image'] = i.find_element(By.TAG_NAME,"img").get_attribute("src")
            modelo_atual['Supermarket'][-1]['Historic'][-1]['Price'] = float(Valor_produto)
            data.append(modelo_atual)
        
        else:
            print("erro")
            print(textos)

    return data


def coleta_atacadao():

    client = pymongo.MongoClient("mongodb+srv://eduardocdiogenes:ikCf850j6u8Pvkwn@cestacheia.9nwfrw2.mongodb.net/?retryWrites=true&w=majority")
    db = client.cestaCheia

    contador = 0

    dados = []

    # Create Chromeoptions instance 
    options = webdriver.ChromeOptions() 
    
    # Adding argument to disable the AutomationControlled flag 
    options.add_argument("--disable-blink-features=AutomationControlled") 
    
    # Exclude the collection of enable-automation switches 
    options.add_experimental_option("excludeSwitches", ["enable-automation"]) 
    
    # Turn-off userAutomationExtension 
    options.add_experimental_option("useAutomationExtension", False) 

    options.add_argument("--headless=new")

    driver = webdriver.Chrome(options)

    # Changing the property of the navigator value for webdriver to undefined 
    driver.execute_script("Object.defineProperty(navigator, 'webdriver', {get: () => undefined})")

    sites_coletados = ['https://www.atacadao.com.br/bebidas/','https://www.atacadao.com.br/mercearia/',
                    'https://www.atacadao.com.br/frios-e-laticinios/','https://www.atacadao.com.br/carnes-aves-e-peixes/'
                    ,'https://www.atacadao.com.br/congelados/','https://www.atacadao.com.br/hortifruti/','https://www.atacadao.com.br/paes-e-bolos/']

    for url in sites_coletados:
        print("getting url", url)
        driver.get(url)
        print("DONE!")
        driver.maximize_window()
        print("WAITING SLEEP")
        time.sleep(20)
        try:
            print("Cancel button....")
            # Aguardar até que o botão "Cancelar" esteja presente e depois clicar nele
            cancel_button = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, "//button[text()='Cancelar']"))
            )
            cancel_button.click()
            print("Cancel button clicked")
        except:
            pass
        valor_antigo = 0
        while True:
            localizacao_inicial = driver.find_elements(By.XPATH,'/html/body/main/section[3]/div[1]/div[2]/div[2]/div[4]/div/div')
            values = [el.location['y'] for el in localizacao_inicial if el.location['y']]
            values.sort()
            localizacao_inicial = values[-1]
            driver.execute_script("window.scrollTo(0,{});".format(localizacao_inicial))
            scroll_position = driver.execute_script("return window.pageYOffset;")
            
            if localizacao_inicial == valor_antigo:
                contador += 1
                if contador == 10:
                    break

            else:
                contador = 0
            valor_antigo = scroll_position

        quantidade_elementos = len(driver.find_elements(By.XPATH,'/html/body/main/section[3]/div[1]/div[2]/div[2]/div[4]/div/div'))

        for elementos in range(1,math.ceil(quantidade_elementos/4)):

            elemento_maior = elementos * 4

            localizacao_inicial = driver.find_elements(By.XPATH,'/html/body/main/section[3]/div[1]/div[2]/div[2]/div[4]/div/div')[(elemento_maior)-1].location['y']
            driver.execute_script("window.scrollTo(100,{});".format(localizacao_inicial))
            time.sleep(0.7)
            dados += coleta(driver,elemento_maior)

    marcas = list(db['Brands'].find({}))

    produtos = dados

    for i in range(len(produtos)):
        for j in marcas:
            try:
                match = re.findall(r'\b{}\b'.format(unidecode(j['Brand'].lower())), unidecode(produtos[i]['Name'].lower()))
            except:
                pass
            else:
                if match:
                    break
        if match:
            produtos[i]['Brand'] = match[0]
        else:
            produtos[i]['Brand'] = 'outros'

    import json

    # Converta cada JSON em uma string:
    str_jsons_list = [json.dumps(item, sort_keys=True) for item in dados]

    # Remova duplicatas convertendo para um set e depois de volta para uma lista:
    unique_str_jsons_list = list(set(str_jsons_list))

    # Converta cada string de volta para um JSON:
    unique_jsons_list = [json.loads(item) for item in unique_str_jsons_list]


    itens = []

    for i in unique_jsons_list:

        lista_similares = []

        Produto_igual = db['Products'].find_one({'Name': i['Name']})

        if Produto_igual:
            id_igual = Produto_igual['_id']

            for j in range(len(Produto_igual['Supermarket'])):
                if Produto_igual['Supermarket'][j]['Name'] == i['Supermarket'][0]['Name']:
                    Produto_igual['Supermarket'][j]['Date'] = i['Supermarket'][0]['Date']
                    Produto_igual['Supermarket'][j]['Price'] = i['Supermarket'][0]['Price']
                    Produto_igual['Supermarket'][j]['Discount'] = i['Supermarket'][0]['Discount']
                    Produto_igual['Supermarket'][j]['Historic'].append(i['Supermarket'][0]['Historic'][0])

                    filter = {'_id': Produto_igual['_id']}
                    print(i)
                    db['Products'].update_one(filter, {"$set" : {'Supermarket':Produto_igual['Supermarket']}})


        elif i['Brand'] != 'Outros':

            for dados_bd in db['Products'].find({'Brand': i['Brand'].lower()}):
                
                match1 = re.findall(r'(\d+)', dados_bd['Name'])
                match2 = re.findall(r'(\d+)', i['Name'])

                sim = cosine_string_similarity(i['Name'].lower(),dados_bd['Name'].lower())

                if sim > 0.60:
                    if set(match1) & set(match2):
                        lista_similares.append({"Produto":dados_bd,"Sim":sim})
                    elif len(match1) == 0 and len(match2) == 0:
                        lista_similares.append({"Produto":dados_bd,"Sim":sim})

            itens.append([i,lista_similares])
        else:

            itens.append([i,lista_similares])



    for i in range(len(itens)):
        for j in range(i+1, len(itens)):
            for k in range(len(itens[i][1])):
                for l in range(len(itens[j][1])):
                    if itens[i][1][k]["Produto"] == itens[j][1][l]["Produto"]:
                        if itens[i][1][k]["Sim"] >= itens[j][1][l]["Sim"]:
                            itens[j][1][l]["Sim"] = 0
                        else:
                            itens[i][1][k]["Sim"] = 0


    itens_upload = []

    for i in itens:
        i_copy = copy.deepcopy(i)  # cria uma cópia profunda de i
        if i_copy[1] == []:
            itens_upload.append(i_copy[0])
        else:
            highest = 0
            aux = ''
            same = False
            for j in i_copy[1]:
                if j['Sim'] > highest:
                    aux = j
                    highest = j['Sim'] 

            if not isinstance(aux, str):

                filter = {'_id': aux['Produto']['_id']}
                Supermarket = aux['Produto']['Supermarket']

                for j in range(len(Supermarket)):
                    if Supermarket[j]['Name'] == i_copy[0]['Supermarket'][0]['Name']:
                        Supermarket[j]['Historic'].append(i_copy[0]['Supermarket'][0]['Historic'][0])
                        Supermarket[j]['Date'] = i_copy[0]['Supermarket'][0]['Date']
                        Supermarket[j]['Price'] = i_copy[0]['Supermarket'][0]['Price']
                        same = True
                        break

                if not same:
                    Supermarket.append(i_copy[0]['Supermarket'][0])
                    break

                db['Products'].update_one(filter, {"$set" : {'Supermarket':Supermarket}})
                    
            

    db['Products'].insert_many(itens_upload)