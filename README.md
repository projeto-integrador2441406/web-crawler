
# Webcrawler de supermercados

Codigos que realizam a leitura dos produtos, atualização dos dados e inserção em um banco de dados do MongoDB utilizando Selenium e o webdriver do edge, cada programa representa um supermercado diferente (São Luiz, Pão de Açucar, Atacadão e Super Lagoa)

Os codigos são divididos em PF1 e PF2, representando as duas versões do projeto finalizador

### Bibliotecas Necessárias:

msedgedriver==117.0.2045.31 \
pymongo==4.3.2 \
selenium ==3.141.0 \
Unidecode ==1.3.6 \
scikit-learn==1.2.0

### Instalação do webdriver do edge:
https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/

### Como Utilizar:

Instalar as bibliotecas e o webdriver do Edge, conectar com o mongoDB e rodar o arquivo